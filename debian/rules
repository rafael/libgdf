#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export LDFLAGS = -Wl,--as-needed

UVER = $(shell LC_ALL=C dpkg-parsechangelog | grep '^Version:' | cut -d ' ' -f 2,2 | cut -d '-' -f 1,1)
H2MVER = $(shell help2man --version | head -1 | sed -e 's/.* //g')

DEB_CMAKE_EXTRA_FLAGS = \
	-DBUILD_SHARED_LIBS:BOOL=ON \
	-DBUILD_TESTING:BOOL=ON

# OCTDIR=$(shell octave-config --oct-site-dir)
# get octave paths (have to build-depend on octave-headers)
# Note: bdepending on octave-pkg-dev since it is not
#       embedding version information ;) so we could backport
#       easily
include /usr/share/octave/debian/defs.make

%:
	dh $@ --buildsystem=cmake

override_dh_auto_configure:
	dh_auto_configure -- \
		$(DEB_CMAKE_EXTRA_FLAGS)

override_dh_auto_build:
	dh_auto_build

	: # Build quick and dirty manpage whenever recent help2man is available
	if dpkg --compare-versions $(H2MVER) ge 1.37.1; then \
	  LD_LIBRARY_PATH=$$(/bin/ls -d $$PWD/obj-*)/libgdf \
		help2man --no-info -S libGDF -s 1 --version-string=$(UVER) \
		-n "Merge multiple GDF files" \
		*/tools/gdf_merger/gdf_merger >| gdf_merger.1; \
	else \
		touch gdf_merger.1; \
	fi

	: # Build Octave bindings
	cd matlab; \
	 for f in *.cpp; do \
		mkoctfile --mex $$f -I../libgdf/include -L$$(/bin/ls -d $$PWD/../obj-*)/libgdf -lGDF; \
	 done


override_dh_auto_install:
	dh_auto_install
	: # Install Octave bindings
	mkdir -p debian/octave-gdf$(OCTDIR)
	install matlab/*.mex debian/octave-gdf$(OCTDIR)
	[ -e /usr/bin/octave-depends ] && octave-depends -poctave-gdf

override_dh_strip:
	dh_strip --dbg-package=libgdf0-dbg

override_dh_clean:
	dh_clean
	rm -f gdf_merger.1
	-cd matlab; rm *.mex *.o


